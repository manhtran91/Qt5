#!/bin/bash

git clone https://github.com/qt/qt5.git
cd qt5
git checkout v5.9.1
git submodule update --init -- \
    qtbase \
    qtdeclarative \
    qtmultimedia \
    qttools \
    qttranslations \
    qtdoc \
    qtrepotools \
    qtqa \
    qtquickcontrols
perl init-repository \
  --force \
  --module-subset=essential
mkdir -p /opt/qt/5.9.1
export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb
./configure \
   -prefix /opt/qt/5.9.1 \
   -release \
   -opensource \
   -confirm-license \
   -c++std c++11 \
   -shared \
   -accessibility \
   -no-qml-debug \
   -nomake examples \
   -nomake tests \
  | tee ../configure.log
make
make install
cd ..
rm -rf qt5
tree -L 2 /opt/qt/5.9.1
cat configure.log
